package main

import (
	"bytes"
	"log"
	"testing"
)

type rwbuffer struct {
	buf     map[int64]*bytes.Buffer
	verbose bool
}

func (rw *rwbuffer) ReadAt(p []byte, offset int64) (n int, err error) {
	entry, found := rw.buf[offset]
	if !found {
		var contents [3]byte
		entry = new(bytes.Buffer)
		rw.buf[offset] = entry
		_, err = entry.Write(contents[:])
	}
	n, err = entry.Read(p)
	if rw.verbose {
		log.Printf("Reading @ 0x%x: %02x\n", offset, p[0])
	}
	return
}

func (rw *rwbuffer) WriteAt(p []byte, offset int64) (n int, err error) {
	entry, found := rw.buf[offset]
	if !found {
		rw.buf[offset] = new(bytes.Buffer)
		entry = rw.buf[offset]
	}
	if rw.verbose {
		log.Printf("Writing @ 0x%x: %02x\n", offset, p[0])
	}
	n, err = entry.Write(p)
	return
}

func TestBitSet(t *testing.T) {
	rw := &rwbuffer{
		buf:     make(map[int64]*bytes.Buffer),
		verbose: testing.Verbose()}
	offset := int(0x160)
	s := newDaq1278Server(offset, rw)
	val, err := s.modifyLines(uint32(0x5aa555), uint32(0xffffff))
	if err != nil {
		t.Error(err)
	}
	if val != 0x5aa555 {
		t.Errorf("Value mismatch: 0x%06x != 0x5aa555", val)
	}
}

func TestBitClear(t *testing.T) {
	rw := &rwbuffer{
		buf:     make(map[int64]*bytes.Buffer),
		verbose: testing.Verbose()}
	offset := int(0x160)
	s := newDaq1278Server(offset, rw)
	_, err := s.modifyLines(uint32(0x5aa555), uint32(0xffffff))
	if err != nil {
		t.Error(err)
	}

	val, err := s.modifyLines(uint32(0), uint32(0x010101))
	if err != nil {
		t.Error(err)
	}

	expected := uint32(0x5aa555) &^ uint32(0x010101)
	if val != expected {
		t.Errorf("Value mismatch: 0x%06x != 0x%06x", val, expected)
	}
}
