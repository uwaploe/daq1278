# DAQ1278 Driver

Linux user-space "driver" for the Parvus DAQ-1278 digital I/O board. This
driver is designed to run as a root-owned service (by Systemd) and
provides a gRPC interface for clients.

This replaces the Dbus-based user-space driver used in the original COVIS
system.
