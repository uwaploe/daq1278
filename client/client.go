// Package main implements a gRPC client that provides access to
// the DIO features of a Parvus DAQ-1278 board.
package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/daq1278/api"
	"github.com/urfave/cli"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var Version = "dev"
var BuildDate = "unknown"

func setlines(client api.Daq1278Client, lines []uint) ([2]uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &api.LinesMsg{Bits: 0}
	for _, l := range lines {
		msg.Bits |= (1 << l)
	}
	resp, err := client.SetLines(ctx, msg)
	if err != nil {
		return [2]uint32{}, err
	}

	return [2]uint32{resp.Inputs, resp.Outputs}, nil
}

func clearlines(client api.Daq1278Client, lines []uint) ([2]uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &api.LinesMsg{Bits: 0}
	for _, l := range lines {
		msg.Bits |= (1 << l)
	}
	resp, err := client.ClearLines(ctx, msg)
	if err != nil {
		return [2]uint32{}, err
	}

	return [2]uint32{resp.Inputs, resp.Outputs}, nil
}

func testlines(client api.Daq1278Client) ([2]uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &api.Empty{}
	resp, err := client.TestLines(ctx, msg)
	if err != nil {
		return [2]uint32{}, err
	}

	return [2]uint32{resp.Inputs, resp.Outputs}, nil
}

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	app := cli.NewApp()
	app.Name = "daq1278"
	app.Usage = "access the DIO features of a DAQ-1278 board"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "server, s",
			Usage:  "Server address, host:port",
			Value:  "127.0.0.1:10000",
			EnvVar: "DAQ1278_SERVER",
		},
		cli.BoolTFlag{
			Name:  "output-only",
			Usage: "Only show the output register",
		},
	}

	var conn *grpc.ClientConn
	var client api.Daq1278Client

	app.Before = func(c *cli.Context) error {
		var err error
		conn, err = grpc.Dial(c.String("server"), opts...)
		if err == nil {
			client = api.NewDaq1278Client(conn)
		}
		return err
	}

	app.After = func(c *cli.Context) error {
		if conn != nil {
			conn.Close()
		}
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:            "set",
			Usage:           "assert output lines",
			SkipFlagParsing: true,
			ArgsUsage:       "line [line ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return fmt.Errorf("Missing arguments")
				}
				lines := make([]uint, 0)
				for i := 0; i < c.NArg(); i++ {
					line, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
					if err != nil {
						return cli.NewExitError(err, 1)
					}
					lines = append(lines, uint(line))
				}
				value, err := setlines(client, lines)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				if c.GlobalBoolT("output-only") {
					fmt.Fprintf(c.App.Writer, "0x%08x\n", value[1])
				} else {
					fmt.Fprintf(c.App.Writer, "0x%08x 0x%08x\n", value[0], value[1])
				}
				return nil
			},
		},
		{
			Name:            "clear",
			Usage:           "deassert output lines",
			SkipFlagParsing: true,
			ArgsUsage:       "line [line ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return fmt.Errorf("Missing arguments")
				}
				lines := make([]uint, 0)
				for i := 0; i < c.NArg(); i++ {
					line, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
					if err != nil {
						return cli.NewExitError(err, 1)
					}
					lines = append(lines, uint(line))
				}
				value, err := clearlines(client, lines)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				if c.GlobalBoolT("output-only") {
					fmt.Fprintf(c.App.Writer, "0x%08x\n", value[1])
				} else {
					fmt.Fprintf(c.App.Writer, "0x%08x 0x%08x\n", value[0], value[1])
				}
				return nil
			},
		},
		{
			Name:            "test",
			Usage:           "read i/o lines",
			SkipFlagParsing: true,
			Action: func(c *cli.Context) error {
				value, err := testlines(client)
				if err != nil {
					return cli.NewExitError(err, 2)
				}

				if c.NArg() < 1 {
					if c.GlobalBoolT("output-only") {
						fmt.Fprintf(c.App.Writer, "0x%08x\n", value[1])
					} else {
						fmt.Fprintf(c.App.Writer, "0x%08x 0x%08x\n", value[0], value[1])
					}
				} else {
					for i := 0; i < c.NArg(); i++ {
						line, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
						if err != nil {
							return cli.NewExitError(err, 1)
						}
						if (value[1] & uint32(1<<uint(line))) != 0 {
							fmt.Fprint(c.App.Writer, "1 ")
						} else {
							fmt.Fprint(c.App.Writer, "0 ")
						}
					}
					fmt.Fprint(c.App.Writer, "\n")
				}

				return nil
			},
		},
	}

	app.Run(os.Args)
}
