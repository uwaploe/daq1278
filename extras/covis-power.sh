#!/usr/bin/env bash
#
# Use the DAQ-1278 gRPC server to manage the power switches for
# the COVIS peripheral devices.
#
declare -A SWITCHES
SWITCHES=(
    ["v48"]="0"
    ["sonar"]="1"
    ["v24"]="2"
    ["tilt"]="3"
    ["roll"]="4"
    ["pan"]="5"
    ["sensor"]="6"
    ["trigger"]="7"
)

power_test ()
{
    local state
    state="$(daq1278_client test $1)"
    (( $state == 1 ))
}

#
# If there are no command-line arguments, print the state
# of every switch
#
if (($# == 0)); then
    for dev in "${!SWITCHES[@]}"; do
        if power_test "${SWITCHES[$dev]}"; then
            echo "${dev}=on"
        else
            echo "${dev}=off"
        fi
    done
    exit 0
fi

op=
for arg; do
    case "$arg" in
        -on) op="set" ;;
        -off) op="clear" ;;
        *)
            sw="${SWITCHES[$arg]}"
            if [[ -n $sw ]]; then
                [[ -n $op ]] &&
                    daq1278_client "$op" "$sw" 1> /dev/null
                if power_test "$sw"; then
                    echo "${arg}=on"
                else
                    echo "${arg}=off"
                fi
            fi
            ;;
    esac
done
