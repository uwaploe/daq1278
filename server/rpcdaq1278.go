// Rpcdaq1278 is a gRPC server that provides access to the DIO lines on a
// Parvus DAQ-1278 board. This server only listens on the loopback
// interface.
package main

import (
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"

	"bitbucket.org/uwaploe/daq1278/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

var Version = "dev"
var BuildDate = "unknown"

const (
	AddrReg int64 = 0
	DataReg int64 = 1
)

var (
	address  = flag.String("address", ":10000", "The server TCP address and port")
	ioaddr   = flag.Int("ioaddr", 0x160, "The I/O port base address")
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
)

type RWAt interface {
	io.ReaderAt
	io.WriterAt
}

type daq1278Server struct {
	fp       RWAt
	offset   int64
	mu       *sync.Mutex
	in_regs  []int
	out_regs []int
}

func (s *daq1278Server) modifyLines(value, mask uint32) (uint32, error) {
	var (
		b      [1]byte
		ovalue uint32
		v, m   uint8
		err    error
	)
	sel := [3]uint32{0xff, 0xff00, 0xff0000}
	shift := [3]uint{0, 8, 16}
	ovalue = 0
	s.mu.Lock()
	for j, i := range s.out_regs {
		b[0] = uint8(i)
		_, err = s.fp.WriteAt(b[:], s.offset+AddrReg)
		if err == nil {
			_, err = s.fp.ReadAt(b[:], s.offset+DataReg)
			if err == nil {
				m = uint8((mask & sel[j]) >> shift[j])
				v = uint8((value & sel[j]) >> shift[j])
				b[0] = (b[0] &^ m) | (v & m)
				_, err = s.fp.WriteAt(b[:], s.offset+DataReg)
				if err != nil {
					break
				}
				ovalue = ovalue | (uint32(b[0]) << shift[j])
			}
		}
	}
	s.mu.Unlock()
	return ovalue, err
}

func (s *daq1278Server) readLines(regs []int) (uint32, error) {
	var (
		b      [1]byte
		ovalue uint32
		err    error
	)
	shift := [3]uint{0, 8, 16}
	ovalue = 0
	s.mu.Lock()
	for j, i := range regs {
		b[0] = uint8(i)
		_, err = s.fp.WriteAt(b[:], s.offset+AddrReg)
		if err == nil {
			_, err = s.fp.ReadAt(b[:], s.offset+DataReg)
			if err == nil {
				ovalue = ovalue | (uint32(b[0]) << shift[j])
			}
		}
	}
	s.mu.Unlock()
	return ovalue, err
}

func newDaq1278Server(offset int, rw RWAt) *daq1278Server {
	return &daq1278Server{
		fp:       rw,
		offset:   int64(offset),
		mu:       &sync.Mutex{},
		in_regs:  []int{0, 2, 4},
		out_regs: []int{1, 3, 5},
	}
}

func newServer(offset int) *daq1278Server {
	f, err := os.OpenFile("/dev/port", os.O_RDWR|os.O_SYNC, 0660)
	if err != nil {
		return nil
	}
	return newDaq1278Server(offset, f)
}

func (s *daq1278Server) SetLines(ctx context.Context,
	msg *api.LinesMsg) (*api.StateMsg, error) {
	value, err := s.modifyLines(msg.Bits, msg.Bits)
	return &api.StateMsg{Outputs: value}, err
}

func (s *daq1278Server) ClearLines(ctx context.Context,
	msg *api.LinesMsg) (*api.StateMsg, error) {
	value, err := s.modifyLines(0, msg.Bits)
	return &api.StateMsg{Outputs: value}, err
}

func (s *daq1278Server) TestLines(ctx context.Context,
	msg *api.Empty) (*api.StateMsg, error) {
	resp := &api.StateMsg{}
	var err error
	resp.Outputs, err = s.readLines(s.out_regs)
	if err == nil {
		resp.Inputs, err = s.readLines(s.in_regs)
	}
	return resp, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "RPC server for DAQ-1278 DIO board.\n\n")
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Build date: %s\n", BuildDate)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	lis, err := net.Listen("tcp", *address)
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	grpcServer := grpc.NewServer()
	api.RegisterDaq1278Server(grpcServer, newServer(*ioaddr))

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			grpclog.Printf("Got signal %v", s)
			grpcServer.GracefulStop()
		}
	}()

	grpcServer.Serve(lis)
}
