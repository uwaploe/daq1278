#
# This makefile is based on one created for a demo gRPC project:
#
#   https://gitlab.com/pantomath-io/demo-grpc/blob/init-makefile/Makefile
#
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

ifeq ($(shell go env GOOS),linux)
GOBUILD := go build -i
else
GOBUILD := CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
endif

SERVER := "daq1278_svc"
CLIENT := "daq1278_client"
API_OUT := "api/api.pb.go"
PKG := "bitbucket.org/uwaploe/daq1278"
SERVER_PKG_BUILD := "$(PKG)/server"
CLIENT_PKG_BUILD := "$(PKG)/client"
PKG_LIST := $(shell go list $(PKG)/... | grep -v /vendor/)

.PHONY: all api build_server build_client

all: build_server build_client

api/api.pb.go: api/api.proto
	@protoc -I api/ \
		-I${GOPATH}/src \
		--go_out=plugins=grpc:api \
		api/api.proto

api: api/api.pb.go ## Auto-generate grpc go sources

dep: ## Get the dependencies
	@go get -v -d ./...

build_server: dep api ## Build gRPC server
	@$(GOBUILD) -v -o $(SERVER) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(SERVER_PKG_BUILD)

build_client: dep api ## Build gRPC client
	@$(GOBUILD) -v -o $(CLIENT) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(CLIENT_PKG_BUILD)

clean: ## Remove previous builds
	@rm -f $(SERVER) $(CLIENT)

distclean: clean ## Remove auto-generated files
	@rm -f $(API_OUT)


help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
	  sort | \
	  awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'