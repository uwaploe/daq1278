// Code generated by protoc-gen-go.
// source: api.proto
// DO NOT EDIT!

/*
Package api is a generated protocol buffer package.

It is generated from these files:
	api.proto

It has these top-level messages:
	Empty
	LinesMsg
	StateMsg
*/
package api

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Empty struct {
}

func (m *Empty) Reset()                    { *m = Empty{} }
func (m *Empty) String() string            { return proto.CompactTextString(m) }
func (*Empty) ProtoMessage()               {}
func (*Empty) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type LinesMsg struct {
	Bits uint32 `protobuf:"varint,1,opt,name=bits" json:"bits,omitempty"`
}

func (m *LinesMsg) Reset()                    { *m = LinesMsg{} }
func (m *LinesMsg) String() string            { return proto.CompactTextString(m) }
func (*LinesMsg) ProtoMessage()               {}
func (*LinesMsg) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *LinesMsg) GetBits() uint32 {
	if m != nil {
		return m.Bits
	}
	return 0
}

type StateMsg struct {
	Inputs  uint32 `protobuf:"varint,1,opt,name=inputs" json:"inputs,omitempty"`
	Outputs uint32 `protobuf:"varint,2,opt,name=outputs" json:"outputs,omitempty"`
}

func (m *StateMsg) Reset()                    { *m = StateMsg{} }
func (m *StateMsg) String() string            { return proto.CompactTextString(m) }
func (*StateMsg) ProtoMessage()               {}
func (*StateMsg) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *StateMsg) GetInputs() uint32 {
	if m != nil {
		return m.Inputs
	}
	return 0
}

func (m *StateMsg) GetOutputs() uint32 {
	if m != nil {
		return m.Outputs
	}
	return 0
}

func init() {
	proto.RegisterType((*Empty)(nil), "api.Empty")
	proto.RegisterType((*LinesMsg)(nil), "api.LinesMsg")
	proto.RegisterType((*StateMsg)(nil), "api.StateMsg")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for Daq1278 service

type Daq1278Client interface {
	SetLines(ctx context.Context, in *LinesMsg, opts ...grpc.CallOption) (*StateMsg, error)
	ClearLines(ctx context.Context, in *LinesMsg, opts ...grpc.CallOption) (*StateMsg, error)
	TestLines(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*StateMsg, error)
}

type daq1278Client struct {
	cc *grpc.ClientConn
}

func NewDaq1278Client(cc *grpc.ClientConn) Daq1278Client {
	return &daq1278Client{cc}
}

func (c *daq1278Client) SetLines(ctx context.Context, in *LinesMsg, opts ...grpc.CallOption) (*StateMsg, error) {
	out := new(StateMsg)
	err := grpc.Invoke(ctx, "/api.Daq1278/SetLines", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *daq1278Client) ClearLines(ctx context.Context, in *LinesMsg, opts ...grpc.CallOption) (*StateMsg, error) {
	out := new(StateMsg)
	err := grpc.Invoke(ctx, "/api.Daq1278/ClearLines", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *daq1278Client) TestLines(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*StateMsg, error) {
	out := new(StateMsg)
	err := grpc.Invoke(ctx, "/api.Daq1278/TestLines", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Daq1278 service

type Daq1278Server interface {
	SetLines(context.Context, *LinesMsg) (*StateMsg, error)
	ClearLines(context.Context, *LinesMsg) (*StateMsg, error)
	TestLines(context.Context, *Empty) (*StateMsg, error)
}

func RegisterDaq1278Server(s *grpc.Server, srv Daq1278Server) {
	s.RegisterService(&_Daq1278_serviceDesc, srv)
}

func _Daq1278_SetLines_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LinesMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(Daq1278Server).SetLines(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Daq1278/SetLines",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(Daq1278Server).SetLines(ctx, req.(*LinesMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Daq1278_ClearLines_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LinesMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(Daq1278Server).ClearLines(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Daq1278/ClearLines",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(Daq1278Server).ClearLines(ctx, req.(*LinesMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Daq1278_TestLines_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(Daq1278Server).TestLines(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Daq1278/TestLines",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(Daq1278Server).TestLines(ctx, req.(*Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _Daq1278_serviceDesc = grpc.ServiceDesc{
	ServiceName: "api.Daq1278",
	HandlerType: (*Daq1278Server)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SetLines",
			Handler:    _Daq1278_SetLines_Handler,
		},
		{
			MethodName: "ClearLines",
			Handler:    _Daq1278_ClearLines_Handler,
		},
		{
			MethodName: "TestLines",
			Handler:    _Daq1278_TestLines_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api.proto",
}

func init() { proto.RegisterFile("api.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 180 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0xe2, 0xe2, 0x4c, 0x2c, 0xc8, 0xd4,
	0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x4e, 0x2c, 0xc8, 0x54, 0x62, 0xe7, 0x62, 0x75, 0xcd,
	0x2d, 0x28, 0xa9, 0x54, 0x92, 0xe3, 0xe2, 0xf0, 0xc9, 0xcc, 0x4b, 0x2d, 0xf6, 0x2d, 0x4e, 0x17,
	0x12, 0xe2, 0x62, 0x49, 0xca, 0x2c, 0x29, 0x96, 0x60, 0x54, 0x60, 0xd4, 0xe0, 0x0d, 0x02, 0xb3,
	0x95, 0x6c, 0xb8, 0x38, 0x82, 0x4b, 0x12, 0x4b, 0x52, 0x41, 0xf2, 0x62, 0x5c, 0x6c, 0x99, 0x79,
	0x05, 0xa5, 0x70, 0x15, 0x50, 0x9e, 0x90, 0x04, 0x17, 0x7b, 0x7e, 0x69, 0x09, 0x58, 0x82, 0x09,
	0x2c, 0x01, 0xe3, 0x1a, 0xf5, 0x32, 0x72, 0xb1, 0xbb, 0x24, 0x16, 0x1a, 0x1a, 0x99, 0x5b, 0x08,
	0x69, 0x71, 0x71, 0x04, 0xa7, 0x96, 0x80, 0x2d, 0x13, 0xe2, 0xd5, 0x03, 0xb9, 0x07, 0x66, 0xb1,
	0x14, 0x84, 0x0b, 0xb3, 0x47, 0x89, 0x41, 0x48, 0x87, 0x8b, 0xcb, 0x39, 0x27, 0x35, 0xb1, 0x88,
	0x38, 0xd5, 0x1a, 0x5c, 0x9c, 0x21, 0xa9, 0xc5, 0x50, 0xa3, 0xb9, 0xc0, 0xb2, 0x60, 0xcf, 0x61,
	0xa8, 0x4c, 0x62, 0x03, 0x07, 0x81, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0x19, 0x5d, 0x60, 0x51,
	0x0f, 0x01, 0x00, 0x00,
}
